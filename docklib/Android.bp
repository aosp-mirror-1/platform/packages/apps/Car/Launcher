//
// Copyright (C) 2023 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

java_library_static {
    name: "dock_item",
    host_supported: true,
    proto: {
        type: "lite",
    },
    sdk_version: "module_current",
    min_sdk_version: "31",
    srcs: ["src/com/android/car/docklib/data/proto/dock_item.proto"],
}

android_library {
    name: "CarDockLib",
    srcs: [
        "src/**/*.java",
        "src/**/*.kt",
    ],

    resource_dirs: ["res"],

    libs: ["android.car"],

    static_libs: [
        "androidx.recyclerview_recyclerview",
        "androidx.core_core-animation",
        "car-ui-lib-no-overlayable",
        "androidx.lifecycle_lifecycle-extensions",
        "com.google.android.material_material",
        "CarDockUtilLib",
        "CarLauncherCommon",
        "SystemUISharedLib",
        "//frameworks/libs/systemui:iconloader",
        "car-resource-common",
        "dock_item",
        "car_launcher_flags_java_lib",
        "car-media-common-no-overlayable",
    ],

    platform_apis: true,

    manifest: "AndroidManifest.xml",
}
